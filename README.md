# Install
For Arch-Based
```
git clone https://gitlab.com/TalkingPanda0/dotfiles.git
cd dotfiles
 p  -r .bin ~/
cp -r .config/* ~/.config/
echo "exec qtile" >> ~/.xinitrc
cd ~
yay -S blueberry qtile rofi picom termite i3lock-fancy qtile nemo nitrogen xclip polkit-gnome materia-gtk-theme qt5-styleplugins lxappearance flameshot pnmixer network-manager-applet xfce4-power-manager -y
wget -qO- https://git.io/papirus-icon-theme-install | sh
echo "Xcursor.theme:   breeze_cursors
Xcursor.size: 16" > ~/.Xresources 
echo "XDG_CURRENT_DESKTOP=Unity
QT_QPA_PLATFORMTHEME=gtk2"  >> /etc/environment
```
For Debian-Based
```
git clone https://gitlab.com/TalkingPanda0/dotfiles.git
cd dotfiles
cp  -r .bin ~/
cp -r .config/* ~/.config/
echo "exec qtile" >> ~/.xinitrc
cd ~
sudo add-apt-repository ppa:regolith-linux/unstable -y
sudo apt install qtile nitrogen termite fonts-roboto rofi picom i3lock nemo xclip qt5-style-plugins materia-gtk-theme lxappearance xbacklight kde-spectacle nautilus xfce4-power-manager pnmixer network-manager-applet gnome-polkit fish -y
wget -qO- https://git.io/papirus-icon-theme-install | sh
echo "Xcursor.theme:  breeze_cursors
Xcursor.size: 16" > ~/.Xresources 
echo "XDG_CURRENT_DESKTOP=Unity
QT_QPA_PLATFORMTHEME=gtk2"  >> /etc/environment
```
After installtation your old config files will be in ~/.config.old
